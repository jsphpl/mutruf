# Mutruf
This is the hardware and software for an arts installation called *Mutruf*. Its purpose is to make people aware of and inform about [Children's rights](https://en.wikipedia.org/wiki/Children%27s_rights).

It uses an old emergency phone from a German Autobahn to play back a pre-recorded message upon pickup. The dissection of the emergecy phone is documented here: [https://jsph.pl/articles/umbau-notrufs%C3%A4ule](https://jsph.pl/articles/umbau-notrufs%C3%A4ule).

Details about the hardware and firmware can be found in their respective subdirectories:
- [hardware](hardware)
- [firmware](firmware)
