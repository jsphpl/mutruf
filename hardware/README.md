# Mutruf Electronics
This is the documentation about the electronics part of the *Mutruf* project.

![front view](images/electronics-front.jpg)
![back view](images/electronics-back.jpg)

## Schematic
![Mutruf Schematic](schematic/mutruf.svg)

KiCad files and PDF schematic can be found in [schematic](schematic) subdirectory.

## BOM
The emergency phone had the following components in it:
- Handset/hook
- 12V lead battery charger
- Solar panel

I added a 12V 7Ah lead battery.

The electronics part is quite simple and requires just a few additional components:

| Component                     | Source                                       | Documentation                                       |
|-------------------------------|----------------------------------------------|-----------------------------------------------------|
| Raspberry Pi Pico             | various                                      | [pico-datasheet.pdf](datasheets/pico-datasheet.pdf) |
| DY-SV8F Voice Playback Module | [eBay](https://www.ebay.de/itm/254322956522) | [dy-sv8f.pdf](datasheets/dy-sv8f.pdf)               |
| DC/DC step-down converter 5V  | various                                      |                                                     |
| 3-pol JST connector           | various                                      |                                                     |
| 2-pol screw terminal          | various                                      |                                                     |
| Perfboard                     | various                                      |                                                     |
