#include "DYPlayerPico.h"
#include "config.h"
#include "hardware/gpio.h"
#include "pico/stdlib.h"
#include <stdio.h>

#ifndef PICO_DEFAULT_LED_PIN
#error Need a board with PICO_DEFAULT_LED_PIN
#else
/**
 * The pin where the LED is connected.
 */
const uint8_t LED_PIN = PICO_DEFAULT_LED_PIN;
#endif

/**
 * The pin where the telephone hook is connected.
 *
 * Must be configured as input with pull-up.
 */
const uint8_t HOOK_PIN = 2;

// Initialize the audio player interface via UART with pins GP4 as TX and GP5 as RX
DY::Player player(uart1, 4, 5);

void busyError() {
    printf("An unrecoverable error occurred. Application stopped.");
    while (true) {
        gpio_put(LED_PIN, 1);
        sleep_ms(100);
        gpio_put(LED_PIN, 0);
        sleep_ms(100);
    }
}

void startPlayback() {
    printf("Playing first file on SD card\n");
    player.playSpecified(1);
}

void stopPlayback() {
    printf("Stopping audio playback\n");
    player.stop();
}

void interruptHandler(uint gpio, uint32_t events) {
    if (gpio != HOOK_PIN) {
        return;
    }

    if ((events & 0x04) && !gpio_get(HOOK_PIN)) {
        // Falling edge
        startPlayback();
    } else if ((events & 0x08) && gpio_get(HOOK_PIN)) {
        // Rising edge
        stopPlayback();
    }
}

int main() {
    // Wire up RP2040 stdio
    stdio_init_all();
    sleep_ms(2000);

    printf("Booting %s v%s\n", APP_NAME, VERSION_STRING);

    printf("Configuring GPIO\n");
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    // Turn on LED as an indicator that we're running
    gpio_put(LED_PIN, 1);

    gpio_init(HOOK_PIN);
    gpio_set_dir(HOOK_PIN, GPIO_IN);
    gpio_set_pulls(HOOK_PIN, true, false);

    printf("Configuring audio player module\n");
    player.begin();       // configure UART
    player.setVolume(30); // 100%
    // printf("Checking communication with audio player module: ");
    // DY::play_state_t state = player.checkPlayState();
    // if (state == DY::PlayState::Fail) {
    //     printf("fail\n");
    //     busyError();
    // } else {
    //     printf("pass\n");
    // }

    printf("Enabling interrupts\n");
    gpio_set_irq_enabled_with_callback(
        HOOK_PIN, GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, true, &interruptHandler);

    while (true) {
        tight_loop_contents();
    }

    return 0;
}
