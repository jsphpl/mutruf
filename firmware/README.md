# Mutruf Firmware
This is the firmware for the Mutruf electronics based on a Raspberry Pi Pico (RP2040). It is written in C/C++ and built using arm-gcc through cmake.

It uses the [dyplayer](https://github.com/SnijderC/dyplayer) library to communicate with the sound module via UART.

## Functional description
When the phone is picked up from the hook, an audio file stored on the SD card is played through the phone speaker and a high-power LED is turned on for visual indication. Playback is stopped upon hangup and the LED turns off.

## Prerequisites
- cmake (>= v3.20)
- gcc-arm (v10.x.x)

See [https://github.com/raspberrypi/pico-sdk](https://github.com/raspberrypi/pico-sdk) for instructions on how to set up a machine to build firmware for the pico.

## Build
```sh
git clone https://gitlab.com/jsphpl/mutruf
cd mutruf
git submodule update --init --recursive
cd build
cmake ../firmware
make
```

## Flash
Connect the pico via USB while holding down the boot button. The pico will appear as a mass storage disk. Drag the `build/mutruf-firmware.uf2` file onto that disk.

Alternatively, connect the pico via picoprobe (or another SWD adapter) and run `make flash`.
